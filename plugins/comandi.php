<?php

/*
NeleBotFramework
	Copyright (C) 2018  PHP-Coders

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if ($typechat == "private") {
	function bot_decode($string) {
		global $config;
		$key = hash('sha256', $config['secret_key']);
		$iv = substr(hash('sha256', $config['secret_iv']), 0, 8);
		$datas = openssl_decrypt(base64_decode($string), $config['encrypt_method'], $key, 0, $iv);
		return $datas;
	}
	function bot_encode($datas) {
		global $config;
		if (is_array($datas)) {
			$datas = json_encode($datas);
		}
		$key = hash('sha256', $config['secret_key']);
		$iv = substr(hash('sha256', $config['secret_iv']), 0, 8);
		$string = str_replace('=', '', base64_encode(openssl_encrypt($datas, $config['encrypt_method'], $key, 0, $iv)));
		return $string;
	}
	function setWebhook($chiavi, $webhook, $max_connections = 30) {
		$args = [
			'url' => $webhook,
			'max_connections' => $max_connections
		];
		$r = sendRequest('https://api.telegram.org/' . $chiavi . '/setWebhook', $args, true);
		$rr = json_decode($r, true);
		return $rr;
	}

	if ($cmd) {
		$tycmd = $cmd;
		if ($u['settings']['c_cache']) {
			dm($chatID, $u['settings']['c_cache']);
		}
	} elseif ($cbdata) {
		if (strpos($cbdata, "cancel.") === 0) {
			unset($u['settings']['newbot']);
			unset($u['settings']['reset_token']);
			db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], 'no');
			$cbdata = str_replace("cancel.", '', $cbdata);
		}
		$tycmd = $cbdata;
	} elseif (($u['settings']['reset_token'] or $u['settings']['newbot']) and $msg) {
		if ($u['settings']['c_cache']) {
			dm($chatID, $u['settings']['c_cache']);
		}
	} else {
		dm($chatID, $msgID);
		die;
	}
	
	if ($msg and !$tycmd) {
		if ($u['settings']['newbot']) {
			$e = explode(':', $msg);
			if (strpos($msg, ':') and strlen($msg) < 200 and is_numeric($e[0])) {
				if (strpos($msg, 'bot') === false) {
					$msg = 'bot' . $msg;
				}
				$bot = getMe($msg);
				if ($bot['id']) {
					dm($chatID, $msgID);
					$username_bot = $bot['username'];
					$char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
					$chiave = substr(str_shuffle($char), 0, 35);
					$r = rand(10, 35);
					if ($r < 20) {
						$chiave[$r] = "-";
					} else {
						$chiave[$r] = "_";
					}
					$key = urlencode($msg);
					$password = urlencode($chiave);
					if ($redis->get("$userID.premium-clone")) {
						if ($u['settings']['premium_clones'] > 0) {
							$u['settings']['premium_clones'] = $u['settings']['premium_clones'] - 1;
							$ispremium = true;
						} else {
							$ispremium = false;
						}
					} else {
						$ispremium = false;
					}
					if (!isset(db_query("SELECT bot_id FROM bots WHERE bot_id = ?", [$bot['id']], true)['bot_id'])) {
						db_query("INSERT INTO bots (bot_id, username_bot, owner_id, status, password, premium) VALUES (?,?,?,?,?,?)", [$bot['id'], $bot['username'], $userID, true, bot_encode($password), json_encode($ispremium)], "no");
					} else {
						db_query("UPDATE bots SET username_bot = ?, owner_id = ?, password = ?, premium = ? WHERE bot_id = ?", [$bot['username'], $userID, bot_encode($password), json_encode($ispremium), $bot['id']], "no");
					}
					$url = "https://clones.masterpoll.xyz/?" . http_build_query([
						"key" => $key,
						"password" => $password
					]);
					if ($ispremium) {
						$max = 70;
					} else {
						$max = 30;
					}
					$r = setWebhook($msg, $url, $max);
					if ($r['ok']) {
						$m = sm($chatID, "<b>✅ Bot created successfully!</b>\nHappy polling with @{$bot['username']}");
						unset($u['settings']['newbot']);
						unset($u['settings']['reset_token']);
						$u['settings']['c_cache'] = $m['result']['message_id'];
						db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], 'no');
						$api = $config['clones_logger'];
						sm($config['clones_logger_chat'], bold("#NewBot \nBot: ") . text_link($bot['first_name'], "t.me/" . $bot['username']) . " - " . code($bot['id']) . bold("\nOwner: ") . code($userID) . bold("\nCreated by: ") . tag());
						die;
					} else {
						$t = "<b>⛔️ Error!</b>\nI was unable to set the Webhook for this Bot, contact @MasterPollSupport.";
					}
				} else {
					$t = "<b>🚫 API Key not found</b>";
				}
			} else {
				$t = "<b>🚫 Invalid API Key!</b>\nExample: <code>243412664:ix5JoqkZWCOGKQY-fatvnV3AI4TcU0mrSep</code>";
			}
		} elseif ($u['settings']['reset_token']) {
			$e = explode(':', $msg);
			if (strpos($msg, ':') and strlen($msg) < 200 and is_numeric($e[0])) {
				if (strpos($msg, 'bot') === false) {
					$msg = 'bot' . $msg;
				}
				$bot = getMe($msg);
				if ($bot['id']) {
					dm($chatID, $msgID);
					$qbot = db_query("SELECT * FROM bots WHERE owner_id = ? and bot_id = ? LIMIT 1", [$userID, round($u['settings']['reset_token'])], true);
					if ($bot['id'] == $u['settings']['reset_token']) {
						$username_bot = $bot['username'];
						$char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
						$chiave = substr(str_shuffle($char), 0, 35);
						$r = rand(10, 35);
						if ($r < 20) {
							$chiave[$r] = "-";
						} else {
							$chiave[$r] = "_";
						}
						$key = urlencode($msg);
						$password = urlencode($chiave);
						if (!isset(db_query("SELECT bot_id FROM bots WHERE owner_id = ? and bot_id = ?", [$userID, $bot['id']], true)['bot_id'])) {
							$t = "<b>🚫 Bot not found, please use /newbot to create it.</b>";
							die;
						} else {
							db_query("UPDATE bots SET username_bot = ?, owner_id = ?, password = ? WHERE bot_id = ?", [$bot['username'], $userID, bot_encode($password), $bot['id']], "no");
						}
						$url = "https://clones.masterpoll.xyz/?" . http_build_query([
							"key" => $key,
							"password" => $password
						]);
						$r = setWebhook($msg, $url);
						if ($r['ok']) {
							unset($u['settings']['newbot']);
							unset($u['settings']['reset_token']);
							if (!$bot['supports_inline_queries']) {
								$m = sm($chatID, "<a href='https://t.me/MasterPollSupport'>&#8203;</a>" . italic("You need to setup the Bot on @BotFather!"), false, 'def', false, false, false);
								$u['settings']['c_cache'] = $m['result']['message_id'];
							}
							db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], 'no');
							$bot = db_query("SELECT * FROM bots WHERE owner_id = ? and bot_id = ? LIMIT 1", [$userID, round($bot['id'])], true);
							$t = bold("@{$bot['username_bot']}") . "\nWhat do you want to do with the bot?";
							if (!json_decode($bot['master_status'], true)) {
								$t .= italic("\n\n🚫 Your Bot @{$bot['username_bot']} has been deactivated from our system by the MasterPoll Staff!");
							}
							$menu[] = [
								[
									"text" => "{$c[$bot['status']]} Status",
									"callback_data" => "status{$bot['bot_id']}"
								],
								[
									"text" => "🔁 Update Data",
									"url" => "https://t.me/{$bot['username_bot']}?start=" . bot_encode("update-data")
								]
							];
							$menu[] = [
								[
									"text" => "🆕 Reset token",
									"callback_data" => "setWebhook{$bot['bot_id']}"
								],
								[
									"text" => "🗑 Delete",
									"callback_data" => "del{$bot['bot_id']}"
								]
							];
							$menu[] = [
								[
									"text" => "🔙 Back",
									"callback_data" => "mybots"
								]
							];
						} else {
							$t = "<b>⛔️ Error!</b>\nI was unable to set the Webhook for this Bot, contact @MasterPollSupport.";
						}
					} else {
						$t = "<b>🚫 This isn't the API Key for @{$qbot['username_bot']}</b>\n\n	 Are you trying to trick me?";
					}
				} else {
					$t = "<b>🚫 API Key not found</b>";
				}
			} else {
				$t = "<b>🚫 Invalid API Key!</b>\nExample: <code>243412664:ix5JoqkZWCOGKQY-fatvnV3AI4TcU0mrSep</code>";
			}
		} else {
			unset($u['settings']['newbot']);
			unset($u['settings']['reset_token']);
			db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], 'no');
			dm($chatID, $msgID);
			die;
		}
	} elseif ($tycmd == "cancel") {
		if ($u['settings']['newbot'] or $u['settings']['reset_token']) {
			unset($u['settings']['newbot']);
			unset($u['settings']['reset_token']);
			db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], 'no');
			$t = "❎ Action canceled...";
		} else {
			$t = "💤 You haven't any active action.";
		}
	} elseif ($tycmd == "bug") {
		if ($u['settings']['newbot'] or $u['settings']['reset_token']) {
			unset($u['settings']['newbot']);
			unset($u['settings']['reset_token']);
			db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], 'no');
		}
		$t = "You want to report bugs on this Bot? Please contact @MasterPollSupport.";
	} elseif ($tycmd == 'start') {
		if ($u['settings']['newbot'] or $u['settings']['reset_token']) {
			unset($u['settings']['newbot']);
			unset($u['settings']['reset_token']);
			if ($u['settings']['c_cache'] !== $cbmid and isset($u['settings']['c_cache'])) {
				dm($chatID, $u['settings']['c_cache']);
			}
			if (is_numeric($cbmid)) $u['settings']['c_cache'] = $cbmid;
			$cbt = "❎ Action canceled...";
			db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], 'no');
		} elseif (is_numeric($cbmid)) {
			if ($u['settings']['c_cache'] !== $cbmid and isset($u['settings']['c_cache'])) {
				dm($chatID, $u['settings']['c_cache']);
			}
			$u['settings']['c_cache'] = $cbmid;
			db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], 'no');
		}
		$menu[] = [
			[
				"text" => "➕ New Bot ➕",
				"callback_data" => "newbot"
			]
		];
		$menu[] = [
			[
				"text" => "🤖 My Bots",
				"callback_data" => "mybots"
			],
			[
				"text" => "ℹ️ Help",
				"callback_data" => "help"
			]
		];
		$t = bold("👮🏻‍♂️ Master PollBot Father") . "\n" . italic("Create and manage clones of @MasterPollBot.");
	} elseif ($tycmd == "help") {
		$menu[] = [
			[
				"text" => "🔙 Back",
				"callback_data" => "start"
			]
		];
		$t = "<b>ℹ️ Help</>\n\n❓ Read the <a href='https://telegra.ph/MasterPoll-FAQ-12-24'>F.A.Q.</>\n⚠️ Report an error on the Bot with /bug.\n\n👥 <a href='https://t.me/MasterPollChat'>MasterPoll | Chat</>\n⭐️ <a href='t.me/MasterPollPremium'>VIP Room</>\n💬 Contact us with @MasterPollSupport.";
	} elseif ($tycmd == "newbot") {
		$bots = db_query("SELECT * FROM bots WHERE owner_id = ? and premium = ?", [$userID, 0], false);
		if (!empty($bots)) {
			$have_free_clone = true;
		} else {
			$have_free_clone = false;
		}
		if ($have_free_clone and $u['settings']['premium_clones'] > 0) {
			$premium = "⭐️";
			$redis->set("$userID.premium-clone", true);
		} elseif ($have_free_clone and $u['settings']['premium_clones'] <= 0) {
			$cbt = "🚫 You've already had a free clone.";
			if ($cbdata) {
				cb_reply($cbid, $cbt, true);
			} else {
				sm($chatID, bold($cbt));
			}
			die;
		} elseif ($u['settings']['premium_clones'] > 0 and $redis->get("$userID.premium-clone")) {
			$premium = "⭐️";
		} else {
			$premium = "❌";
		}
		$menu[] = [
			[
				"text" => "🔙 Back",
				"callback_data" => "start"
			],
			[
				"text" => "$premium Premium",
				"callback_data" => "newbot.premium"
			]
		];
		if ($have_free_clone and !$u['settings']['premium_clones'] and !$isadmin) {
			$cbt = "🚫 You've already had a free clone.";
		} else {
			$u['settings']['newbot'] = true;
			db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], 'no');
			$t = "<b>🆕 New Bot</b>\nCreate a Bot using @BotFather, then send here the token.";
		}
	} elseif ($tycmd == "newbot.premium") {
		if ($u['settings']['premium_clones'] > 0) {
			$bots = db_query("SELECT * FROM bots WHERE owner_id = ? and premium = ?", [$userID, "false"], false);
			if (!empty($bots)) {
				cb_reply($cbid, '🚫 You can only have 1 free Bot!', true);
				die;
			}
			if ($redis->get("$userID.premium-clone")) {
				$premium = "❌";
				$redis->del("$userID.premium-clone");
			} else {
				$premium = "⭐️";
				$redis->set("$userID.premium-clone", true);
			}
			$menu[] = [
				[
					"text" => "🔙 Back",
					"callback_data" => "start"
				],
				[
					"text" => "$premium Premium",
					"callback_data" => "newbot.premium"
				]
			];
			$bots = db_query("SELECT * FROM bots WHERE owner_id = ? and premium = ?", [$userID, "false"], false);
			if (!empty($bots)) {
				$have_free_clone = true;
			}
			if ($have_free_clone and !$isadmin) {
				$cbt = "🚫 You've already had a free clone.";
			} else {
				$u['settings']['newbot'] = true;
				db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], 'no');
				$t = "<b>🆕 New Bot</b>\nCreate a Bot using @BotFather, then send here the token.";
			}
		} else {
			$menu[] = [
				[
					"text" => "🔙 Back",
					"callback_data" => "newbot"
				],
				[
					"text" => "🛒 Shop",
					"url" => "https://t.me/MasterPoll2Bot?start=shop"
				]
			];
			$t = "<b>⛔️ You haven't any Premium Bot yet!</b>\n	Buy a Premium Bot from our Shop!";
		}
	} elseif ($tycmd == "mybots") {
		$bots = db_query("SELECT * FROM bots WHERE owner_id = ?", [$userID], false);
		if (!empty($bots)) {
			foreach ($bots as $bot) {
				if ($mmenu) {
					$menu[] = [
						$mmenu,
						[
							"text" => "@{$bot['username_bot']}",
							"callback_data" => "bot{$bot['bot_id']}"
						]
					];
					unset($mmenu);
				} else {
					$mmenu = [
						"text" => "@{$bot['username_bot']}",
						"callback_data" => "bot{$bot['bot_id']}"
					];
				}
			}
			if ($mmenu) $menu[] = [$mmenu];
			$botlist = "Choose a Bot from the list below:";
		} else {
			$botlist = "How much I wish I could show you nothing, but I still can't do it...\n	Send /newbot to create a new MasterPoll CloneBot";
		}
		$menu[] = [
			[
				"text" => "🔙 Back",
				"callback_data" => "start"
			]
		];
		$t = "<b>🤖 Bot list</>\n$botlist";
	} elseif (strpos($tycmd, "bot") === 0) {
		$bot_id = str_replace("bot", '', $tycmd);
		if (is_numeric($bot_id)) {
			$bot = db_query("SELECT * FROM bots WHERE owner_id = ? and bot_id = ? LIMIT 1", [$userID, round($bot_id)], true);
			if (round($bot['bot_id']) == round($bot_id)) {
				if (!json_decode($bot['master_status'], true)) {
					$status = "🟡 Deactived";
				} elseif (!json_decode($bot['status'], true)) {
					$status = "🔴 Off";
				} else {
					$status = "🟢 On";
				}
				if (!json_decode($bot['premium'], true)) {
					$premium = "🚫 Not Premium";
				} else {
					$premium = "⭐️ Premium";
				}
				$t = bold("@{$bot['username_bot']}") . "\n<b>Status:</b> $status\n$premium\n\nWhat do you want to do with the Bot?";
				if (!json_decode($bot['master_status'], true)) {
					$t .= italic("\n\n🚫 Your Bot @{$bot['username_bot']} has been deactivated from our system by the MasterPoll Staff!");
				}
				$menu[] = [
					[
						"text" => "{$c[$bot['status']]} Status",
						"callback_data" => "status$bot_id"
					],
					[
						"text" => "🔁 Update Data",
						"url" => "https://t.me/{$bot['username_bot']}?start=" . bot_encode("update-data")
					]
				];
				$menu[] = [
					[
						"text" => "🆕 Reset token",
						"callback_data" => "setWebhook$bot_id"
					],
					[
						"text" => "🗑 Delete",
						"callback_data" => "del$bot_id"
					]
				];
			} else {
				$t = "Why are you trying to view nothing?\n	This Bot doesn't exist anymore...";
			}
		} else {
			$t = "Why are you trying to view nothing?\n	This Bot doesn't exist anymore...";
		}
		$menu[] = [
			[
				"text" => "🔙 Back",
				"callback_data" => "mybots"
			]
		];
	} elseif (strpos($tycmd, "setWebhook") === 0) {
		$bot_id = str_replace("setWebhook", '', $tycmd);
		if (is_numeric($bot_id)) {
			$bot = db_query("SELECT * FROM bots WHERE owner_id = ? and bot_id = ? LIMIT 1", [$userID, round($bot_id)], true);
			if (round($bot['bot_id']) == round($bot_id)) {
				$u['settings']['reset_token'] = $bot_id;
				db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], 'no');
				$t = "Now send me the new token of your Bot @{$bot['username_bot']}";
			} else {
				$t = "Why are you trying to view nothing?\n	This Bot doesn't exist anymore...";
			}
		} else {
			$t = "Why are you trying to view nothing?\n	This Bot doesn't exist anymore...";
		}
		$menu[] = [
			[
				"text" => "🔙 Back",
				"callback_data" => "cancel.bot$bot_id"
			]
		];
	} elseif (strpos($tycmd, "status") === 0) {
		$bot_id = str_replace("status", '', $tycmd);
		if (is_numeric($bot_id)) {
			$bot = db_query("SELECT * FROM bots WHERE owner_id = ? and bot_id = ? LIMIT 1", [$userID, round($bot_id)], true);
			if (round($bot['bot_id']) == round($bot_id)) {
				$bool = [
					null => true,
					true => false,
					false => true,
					"true" => "false",
					"false" => "true"
				];
				$bot['status'] = $bool[$bot['status']];
				db_query("UPDATE bots SET status = ? WHERE owner_id = ? and bot_id = ?", [$bot['status'], $userID, $bot['bot_id']], 'no');
				if (!json_decode($bot['master_status'], true)) {
					$status = "🟡 Deactived";
				} elseif (!json_decode($bot['status'], true)) {
					$status = "🔴 Off";
				} else {
					$status = "🟢 On";
				}
				if (!json_decode($bot['premium'], true)) {
					$premium = "🚫 Not Premium";
				} else {
					$premium = "⭐️ Premium";
				}
				$t = bold("@{$bot['username_bot']}") . "\n<b>Status:</b> $status\n$premium\n\nWhat do you want to do with the Bot?";
				if (!json_decode($bot['master_status'], true)) {
					$t .= italic("\n\n🚫 Your Bot @{$bot['username_bot']} has been deactivated from our system by the MasterPoll Staff!");
				}
				$menu[] = [
					[
						"text" => "{$c[$bot['status']]} Status",
						"callback_data" => "status$bot_id"
					],
					[
						"text" => "🔁 Update Data",
						"url" => "https://t.me/{$bot['username_bot']}?start=" . bot_encode("update-data")
					]
				];
				$menu[] = [
					[
						"text" => "🆕 Reset token",
						"callback_data" => "setWebhook$bot_id"
					],
					[
						"text" => "🗑 Delete",
						"callback_data" => "del$bot_id"
					]
				];
			} else {
				$t = "Why are you trying to view nothing?\n	This Bot doesn't exist anymore...";
			}
		} else {
			$t = "Why are you trying to view nothing?\n	This Bot doesn't exist anymore...";
		}
		$menu[] = [
			[
				"text" => "🔙 Back",
				"callback_data" => "mybots"
			]
		];
	} elseif (strpos($tycmd, "delete") === 0) {
		$bot_id = str_replace("delete", '', $tycmd);
		if (is_numeric($bot_id)) {
			$bot = db_query("SELECT * FROM bots WHERE owner_id = ? and bot_id = ? LIMIT 1", [$userID, round($bot_id)], true);
			if (round($bot['bot_id']) == round($bot_id)) {
				db_query("DELETE FROM bots WHERE owner_id = ? and bot_id = ?", [$userID, $bot['bot_id']], 'no');
				if ($bot['premium']) {
					$u['settings']['premium_clones'] += 1;
					db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], 'no');
				}
				$bots = db_query("SELECT * FROM bots WHERE owner_id = ?", [$userID], false);
				if (!empty($bots)) {
					foreach ($bots as $a) {
						if ($mmenu) {
							$menu[] = [
								$mmenu,
								[
									"text" => "@{$a['username_bot']}",
									"callback_data" => "bot{$a['bot_id']}"
								]
							];
							unset($mmenu);
						} else {
							$mmenu = [
								"text" => "@{$a['username_bot']}",
								"callback_data" => "bot{$a['bot_id']}"
							];
						}
					}
					if ($mmenu) $menu[] = [$mmenu];
					$botlist = "Choose a Bot from the list below:";
				} else {
					$botlist = "How much I wish I could show you nothing, but I still can't do it...\n	Send /newbot to create a new MasterPoll CloneBot";
				}
				$menu[] = [
					[
						"text" => "🔙 Back",
						"callback_data" => "start"
					]
				];
				$t = "<b>🤖 Bot list</>\n$botlist";
				if ($cmd) {
					sm($chatID, $t, $menu);
				} elseif ($cbdata) {
					cb_reply($cbid, $cbt, false, $cbmid, $t, $menu);
				}
				$api = $config['clones_logger'];
				sm($config['clones_logger_chat'], bold("#Deleted \nBot: ") . text_link("@" . $bot['username_bot'], "t.me/" . $bot['username_bot']) . " - " . code($bot['bot_id']) . bold("\nOwner: ") . code($userID) . bold("\nDeleted by: ") . tag());
				die;
			} else {
				$t = "Why are you trying to view nothing?\n	This Bot doesn't exist anymore...";
			}
		} else {
			$t = "Why are you trying to view nothing?\n	This Bot doesn't exist anymore...";
		}
		$menu[] = [
			[
				"text" => "🔙 Back",
				"callback_data" => "cancel.mybots"
			]
		];
	} elseif (strpos($tycmd, "del") === 0) {
		$bot_id = str_replace("del", '', $tycmd);
		if (is_numeric($bot_id)) {
			$bot = db_query("SELECT * FROM bots WHERE owner_id = ? and bot_id = ? LIMIT 1", [$userID, round($bot_id)], true);
			if (round($bot['bot_id']) == round($bot_id)) {
				$t = "You're about to delete your Bot @{$bot['username_bot']}.\nIs this correct?";
				$r = rand(0,2);
				$nope = [
					"No",
					"Nope",
					"No, nevermind!"
				];
				foreach(range(0, 2) as $num) {
					if ($num == $r) {
						$menu[] = [
							[
								"text" => "Yes, delete the bot",
								"callback_data" => "delete$bot_id"
							]
						];
					} else {
						$rn = rand(0, count($nope) - 1);
						$menu[] = [
							[
								"text" => $nope[$rn],
								"callback_data" => "bot$bot_id"
							]
						];
						$nope = array_values(array_diff($nope, [$nope[$rn]]));
					}
				}
			} else {
				$t = "Why are you trying to view nothing?\n	This Bot doesn't exist anymore...";
			}
		} else {
			$t = "Why are you trying to view nothing?\n	This Bot doesn't exist anymore...";
		}
		if (!isset($menu)) {
			$menu[] = [
				[
					"text" => "🔙 Back",
					"callback_data" => "mybots"
				]
			];
		}
	} else {
		$t = "❎ Sorry Sir or Madam, I can't understand you...";
	}
	if ($cmd or $msg) {
		$m = sm($chatID, $t, $menu);
		dm($chatID, $msgID);
		$u['settings']['c_cache'] = $m['result']['message_id'];
		db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], 'no');
	} elseif ($cbdata) {
		if (is_numeric($cbmid)) {
			if ($u['settings']['c_cache'] !== $cbmid and isset($u['settings']['c_cache'])) {
				dm($chatID, $u['settings']['c_cache']);
			}
			$u['settings']['c_cache'] = $cbmid;
			db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], 'no');
		}
		if ($t) {
			cb_reply($cbid, $cbt, false, $cbmid, $t, $menu);
		} elseif ($cbt) {
			cb_reply($cbid, $cbt, true);
		} else {
			cb_reply($cbid);
		}
	}
}
