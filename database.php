<?php

/*
NeleBotFramework
	Copyright (C) 2018	PHP-Coders

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.	If not, see <http://www.gnu.org/licenses/>.
*/

# Connessione a Redis
$times['redis'] = microtime(true);
if ($config['usa_redis']) {
	// Configurazioni di Redis per l'accesso ai Database
	$redisc = $config['redis'];
	// Connessione a Redis
	try {
		$redis = new Redis();
		$redis->connect($redisc['host'], $redisc['port']);
	} catch (Exception $e) {
		botlog($e->getMessage(), $f['database'], 'redis');
		die();
	}
	// Autenticazione
	if ($redisc['password'] !== false) {
		try {
			$redis->auth($redisc['password']);
		} catch (Exception $e) {
			botlog($e, 'redis');
			die();
		}
	}
	// Selezione del database Redis
	if ($redisc['database'] !== false) {
		try {
			$redis->select($redisc['database']);
		} catch (Exception $e) {
			botlog($e, 'redis');
			die();
		}
	}
	// Anti-Ripetizione delle stesse update
	if ($redis->get($update['update_id'] . "-$botID")) {
		botlog("L'Update " . $update['update_id'] . "-$botID" . " è stata ripetuta.", ['framework', 'tg_updates']);
		die;
	} else {
		$redis->set($update['update_id'] . "-$botID", true);
	}
	// Test funzionamento
	if ($cmd == "redis" and $isadmin) {
		$redisping_start = microtime(true);
		$test = $redis->ping();
		$redisping_end = microtime(true);
		if ($test == "+PONG") {
			$res = "✅";
		} else {
			$res = "⚠";
		}
		sm($chatID, bold("Risultato del Test: ") . $res . "\n<b>Ping:</b> " . number_format($redisping_end - $redisping_start, 10));
		die;
	}
	// Elimina una variabile su Redis
	if (strpos($cmd, "redis_del ") === 0 and $isadmin) {
		$obj = str_replace("redis_del ", '', $cmd);
		$redis->del($obj);
		sm($chatID, "Fatto");
		die;
	}
	// Flush Redis
	if ($cmd == "redis_flush" and $isadmin) {
		$redis->flushDb();
		sm($chatID, "✅ Fatto");
		die;
	}
	// Anti-Flood del Bot
	if ($config['usa_il_db']) require($f['anti-flood']);
} else {
	if ($cmd == "redis" and $isadmin) {
		sm($chatID, bold("Risultato del Test: ") . "❌\nRedis è disattivato!");
		die;
	}
}

# Connessione al Database
$times['database'] = microtime(true);
if ($config['usa_il_db']) {
	if (strtolower($database['type']) == 'mysql') {
		try {
			$PDO = new PDO("mysql:host=" . $database['host'] . ";dbname=" . $database['nome_database'] . ";charset=utf8mb4", $database['utente'], $database['password']);
		} catch (PDOException $e) {
			botlog($e->getMessage(), ['pdo', 'database'], $f['database']);
			die;
		}
	} elseif (strtolower($database['type']) == 'sqlite') {
		try {
			$PDO = new PDO("sqlite:" . $database['nome_database']);
		} catch (PDOException $e) {
			botlog($e->getMessage(), ['pdo', 'database'], $f['database']);
			die;
		}
	} elseif (strtolower($database['type']) == 'postgre') {
		try {
			$PDO = new PDO("pgsql:host=" . $database['host'] . ";dbname=" . $database['nome_database'], $database['utente'], $database['password']);
		} catch (PDOException $e) {
			botlog($e->getMessage(), ['pdo', 'database'], $f['database']);
			die;
		}
	} else {
		botlog("Errore: tipo di database sconosciuto.", ['database', 'framework'], $f['database']);
		die;
	}
	
	if ($isadmin and strtolower($cmd) == $database['type']) {
		$dbping_start = microtime(true);
		$test = db_query("SELECT * FROM utenti WHERE user_id = ?", [$userID], true);
		$dbping_end = microtime(true);
		if ($test['user_id']) {
			$res = "✅";
		} else {
			$res = "⚠";
		}
		sm($chatID, bold("Risultato del Test: ") . "$res\n<b>Ping:</b> " . number_format($dbping_end - $dbping_start, 10));
		die;
	}
	
	# Database di Utenti
	if ($userID) {
		# Ban dal Bot
		if (is_int($banFromAntiflood)) {
			$dateban = time() + $banFromAntiflood;
			foreach (array_keys($config['cloni']) as $idBot) {
				$new[$idBot] = "ban$dateban";
			}
			db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode($new), $userID], "no");
			die;
		}
		$u = db_query("SELECT * FROM utenti WHERE user_id = ? LIMIT 1", [$userID], true);
		if (!$cognome) {
			$cognome = "";
		}
		if (!$username) {
			$username = "";
		} else {
			$hausername = "\n<b>Username:</> @$username";
		}
		if (!isset($lingua)) {
			$lingua = "en";
		} else {
			$lingua = explode("-", $lingua)[0];
			$rest = [
				'root' => 'en',
				'rus' => 'ru',
				'uzb' => 'uz',
				'zh' => 'zh_TW',
				'nb_NO' => 'nb',
				'gsw' => 'de'
			];
			if (in_array($lingua, array_keys($rest))) {
				$lingua = $rest[$lingua];
			}
		}
		if (!$u['user_id']) {
			$first_start = true;
			unset($new);
			if ($is_bot) {
				foreach (array_keys($config['cloni']) as $idBot) {
					$new[$idBot] = "bot";
				}
			} else {
				$new[$botID] = "attivo";
			}
			db_query("INSERT INTO utenti (user_id, nome, cognome, username, lang, page, status, last_update, first_update) VALUES (?,?,?,?,?,?,?,?,?)", [$userID, $nome, $cognome, $username, $lingua, '', json_encode($new), time(), time()], 'no');
			$u = db_query("SELECT * FROM utenti WHERE user_id = ? LIMIT 1", [$userID], true);
		}
		if ($u) {
			if ($nome !== $u['nome'] or $cognome !== $u['cognome'] or $username !== $u['username']) {
				$u['nome'] = $nome;
				$u['cognome'] = $cognome;
				$u['username'] = $username;
				db_query("UPDATE utenti SET nome = ?, cognome = ?, username = ? WHERE user_id = ?", [$nome, $cognome, $username, $userID], 'no');
			}
			$u['settings'] = json_decode($u['settings'], true);
			if (!is_array($u['settings'])) {
				$u['settings'] = [];
				$update_settings = true;
			}
			$u['status'] = json_decode($u['status'], true);
			if (!is_array($u['status'])) {
				unset($new);
				if ($is_bot) {
					foreach (array_keys($config['cloni']) as $idBot) {
						$new[$idBot] = "bot";
					}
				} else {
					$new[$botID] = "attivo";
				}
				$u['status'] = $new;
				db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode($u['status']), $userID], "no");
			}
			if ($typechat == "private" and strpos($u['status'][$botID], "ban") !== 0 and $u['status'][$botID] !== "bot") {
				if (in_array($u['status'][$botID], ["attivo", "wait", "inattesa", "blocked"])) {
					$is_new = true;
					$u['status'][$botID] = "avviato";
					db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode($u['status']), $userID], "no");
				}
			}
			if ($u['last_update'] + 60 < time()) {
				$u['last_update'] = time();
				db_query("UPDATE utenti SET last_update = ? WHERE user_id = ?", [$u['last_update'], $userID], "no");
			}
		} else {
			botlog("Utente non caricato sul database: $userID", ['database'], $f['database']);
			die;
		}
	}
	
	# Datbase di Gruppi e Canali
	if ($chatID < 0) {
		die;
	}
	
	# Comandi sul Database solo per Amministratori del Bot
	if($isadmin) {
		# JSON del Utente sul Database
		if ($cmd == "database") {
			if (in_array($typechat, ["supergroup", "group"])) {
				sm($chatID, code(json_encode($g, JSON_PRETTY_PRINT)));
			} else {
				sm($chatID, code(json_encode($u, JSON_PRETTY_PRINT)));
			}
			die;
		}
		# Test degli errori sul Database
		if ($cmd == "dberror") {
			$error = db_query("TEST ERRORI AL DB", false, false);
			sm($chatID, json_encode($error));
			die;
		}
		# Query manuali
		if (strpos($cmd, "query ") === 0) {
			$query = str_replace("query ", '', $cmd);
			$r = db_query($query, false, false);
			sm($chatID, bold("Query: ") . code($query) . "\n" . bold("Risultato: ") . code(substr(json_encode($r), 0, 2047)));
			die;
		}
	}
	
	# Ban dal Bot
	if (!$isadmin) {
		if (strpos($u['status'][$botID], "ban") === 0) {
			if ($u['status'][$botID] == "ban") {
				// Ban a durata non specificata
			} else {
				$time = str_replace("ban", '', $u['status'][$botID]);
				if ($time < time()) {
					if ($config['usa_redis']) {
						$redis->del($userID);
					}
					$u['status'][$botID] = 'attivo';
					db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode($u['status']), $userID]);
					sm($config['console'], "#UnBan #id$userID \n" . bold("Utente unbannato: ") . tag() . " [" . code($userID) . "] \n" . bold("Data: ") . date("d/m/Y h:i:s") . bold("\nUnban automatico"));
				}
			}
			die; // Ban per un utente
		}
		if (strpos($g['status'][$botID], "ban") === 0) {
			if ($g['status'][$botID] == "ban") {
				// Ban a durata non specificata
			} else {
				$time = str_replace("ban", '', $g['status'][$botID]);
				if ($time < time()) {
					if ($config['usa_redis']) {
						$redis->del($chatID);
					}
					$g['status'][$botID] = 'attivo';
					db_query("UPDATE gruppi SET status = ? WHERE chat_id = ?", [json_encode($g['status']), $chatID]);
					sm($chatID, "Gruppo sbannato dal Bot");
					sm($config['console'], "Gruppo sbannato: " . bold("$title") . " [" . code($chatID) . "]");
				}
			}
			lc($chatID);
			die; // Ban per un gruppo
		}
		if (strpos($c['status'][$botID], "ban") === 0) {
			if ($c['status'][$botID] == "ban") {
				// Ban a durata non specificata
			} else {
				$time = str_replace("ban", '', $g['status'][$botID]);
				if ($time < time()) {
					if ($config['usa_redis']) {
						$redis->del($chatID);
					}
					$c['status'][$botID] = 'attivo';
					db_query("UPDATE canali SET status = ? WHERE chat_id = ?", [json_encode($c['status']), $chatID]);
					sm($config['console'], "Canale sbannato: " . bold("$title") . " [" . code($chatID) . "]");
				}
			}
			lc($chatID);
			die; // Ban per un canale
		}
	}
	
} else {
	if ($cmd == "database" and $isadmin) {
		sm($chatID, "Database non attivo");
		die;
	}
}
