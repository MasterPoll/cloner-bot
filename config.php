<?php

/*
NeleBotFramework
	Copyright (C) 2018  PHP-Coders

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

# Impostazioni e Configurazioni del Bot

$config = [
	# Configurazione del bot
	'cloni' => [
		1109278576 => "MPClonesBot"
	],
	// Metti qui l'ID che indica l'username di ogni Bot (senza @)
	'password' => "pdWtZ8bVQNeXh0wglxS19a4oBrEK7P6Tz_G",
	// Password delle request | Lascia false per disabilitarla oppure scrivi una password
	'admins' => [
		244432022, // Nele
		836296867 // Also
	],
	// ID degli Amministratori del Bot
	'devmode' => false,
	// Developer Mode | Attivala quando il Bot è in fase di testing
	'method' => 'post',
	// Scegli get o post come metodo delle request cURL | Uso Default sulle funzioni
	'response' => false,
	// Risposta alla request | Uso Default sulle funzioni
	'request_timeout' => 2,
	// Timeout dell richiesta
	'json_payload' => false,
	// Json Payload è valido solo per le request senza response
	'usa_il_db' => true,
	// Decidi se usare il Database | Consigliato l'uso del Database solo dopo i primi test
	'usa_redis' => true,
	// Decidi se usare Redis
	'logs' => false,
	// Memorizza le updates del tuo Bot (sconsigliato l'utilizzo)
	'log_report' => [
		'SHUTDOWN' => true,
		'FATAL' => true,
		'ERROR' => true,
		'WARN' => true,
		'INFO' => false,
		'DEBUG' => false
	],
	// Segnalazione errori
	'not_log_report' => [],
	// Array di Log da non reportare (Esempio: ['redis'])
	'console' => 244432022,
	// Inserisci l'ID della chat in cui vuoi mandare i log importanti e gli errori (Cambia quello già inserito) oppure metti false per non utilizzarlo.
	'whitelist_users' => false,
	// Blocca gli utenti sconosciuti | Admins auto-inclusi
	'whitelist_chats' => false,
	// Blocca le chat sconosciute (gruppi, supergruppi e canali)
	// false: Whitelist spenta
	// Array: Whitelist accesa
	
	# Impostazioni messaggi
	'operatori_comandi' => ['/', '!', '.', '>'], 
	// Utilizzo comandi del Bot (Es: /cmd !cmd .cmd)
	'post_canali' => true,
	// Decidi se i comandi funzionano anche per i messaggi dai canali
	'modificato' => true,
	// Decidi se i comandi funzionano anche per i messaggi modificati
	'azioni' => false,
	// Sta scrivendo... prima di inviare un messaggio | Uso Default sulle funzioni
	'parse_mode' => "HTML",
	// Scegli HTML o Markdown | Uso Default sulle funzioni
	'disabilita_notifica' => false,
	// Disabilita la notifica dei messaggi | Uso Default sulle funzioni
	'disabilita_anteprima_link' => true,
	// Disabilita l'anteprima dei link | Uso Default sulle funzioni
	
	'clones_logger' => "bot1244848329:AAHiFgd2q9bVInJlQ9AA-EYfO8m1dDKgl6g",
	'clones_logger_chat' => -1001217903505,

    'encrypt_method' => "BF-OFB",
    'secret_key' => 'zx315g43fq30f3zf3fs',
    'secret_iv' => 'gs3116g5es6s8',
	
	'version' => "2.6.4"
	// Versione di NeleBot
];
if (!isset($config['cloni'][$botID])) {
	if ($config['devmode']) {
		die("Bot non autorizzato.");
	} else {
		header("Content-Type: application/json");
		echo json_encode(['method' => 'deleteWebhook']);
		die;
	}
} else {
	$config['username_bot'] = $config['cloni'][$botID];
}

# Dati di accesso al database
$database = [
	'type' => "postgre",
	// Inserisci 'sqlite', 'mysql' o 'postgre' per scegliere il tipo di Database
	'nome_database' => "pollbot",
	// Nome del database (o nome del file per SQLite)
	'utente' => "pollbot",
	// Se usi un' altro utente, sostituisci root con il nome.
	'password' => "BFTs-rnc-rOxbiEIVlAZijKn5dSNhnv8",
	// Password del utente di accesso
	'host' => "localhost"
	// IP/dominio del server mysql (lascia invariato se non sai di cosa si tratta)
];

# Dati di accesso a Redis
$config['redis'] = [
	'database' => 1798,
	// Database da selezionare per l'uso di Redis. Lascia false per usare root.
	'host' => "localhost",
	// Host di connessione al Server Redis.
	'port' => 6379,
	// Porta di connessione al Server Redis.
	'password' => false
	// Password di autenticazione al Server Redis. Lascia false per disattivarlo.
];
